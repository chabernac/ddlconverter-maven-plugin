package chabernac.dbconverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugin.AbstractMojo;

@Mojo(name = "oracle2hsql", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class Oracle2HSQL extends AbstractMojo {
	@Parameter(property = "ddlFile", required = true)
	private File ddlFile;

	@Parameter(property = "hsqlDdlFile", required = true)
	private File hsqlDdlFile;

	public void execute() throws MojoExecutionException {
		hsqlDdlFile.getParentFile().mkdirs();
		try (BufferedReader theReader = new BufferedReader(new InputStreamReader(new FileInputStream(ddlFile)));
				PrintWriter theWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(hsqlDdlFile)))) {
			String theLine = null;
			while ((theLine = theReader.readLine()) != null) {
				theWriter.println(convertLine(theLine));
			}
		} catch (IOException e) {
			throw new MojoExecutionException("Could not convert ddl file", e);
		}
	}

	private String convertLine(String aLine) {
	    aLine = aLine.toLowerCase();
		aLine = aLine.replaceAll("alter table ([a-z_]*) rename column ([a-z_]*) to ([a-z_]*)", "alter table $1 alter column $2 rename to $3");
		aLine = aLine.replaceAll("drop table ([a-z_]*) cascade constraints", "drop table $1 if exists cascade");
		aLine = aLine.replaceAll("alter table ([a-z_]*) modify ([a-z_]*) (.*)", "alter table $1 alter column $2 $3");
		aLine = aLine.replaceAll("drop sequence ([a-z_]*)", "drop sequence $1 if exists");
		aLine = aLine.replaceAll("alter table ([a-z_]*) alter column ([a-z_]*) not null", "alter table $1 alter column $2 set not null");
		
		return aLine;
	}
}
